import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DataTypes

object Main extends App {

  val logger : Logger = Logger.getLogger(Main.getClass)
  Logger.getLogger("org").setLevel(Level.ERROR)
  Logger.getLogger("akka").setLevel(Level.OFF)
  logger.setLevel(Level.ERROR)

  val spark = SparkSession
    .builder
    .appName("Exercise")
    .master("local[*]")
    .getOrCreate()

  val input = spark.sqlContext.read.json("/home/luis/Dropbox/Nova pasta" +
    "/daltix challenge/data/*")

  val product = input
    .withColumn("product", explode(col("products")))
    .drop("products")
    .select("product.*")
    .na.drop(Seq("id", "downloaded_on"))
    .dropDuplicates("id", "downloaded_on")

  val productWithRelevantFields = product
    .select("shop", "location", "country", "id", "eans", "categories",
      "downloaded_on", "pricing")

  val pricing = productWithRelevantFields
    .withColumn("pricing_ind", explode_outer(col("pricing")))
    .drop("pricing")

  val price = pricing
    .withColumn("price", explode_outer(col("pricing_ind.prices")))
    .withColumn("ean", col("eans").getItem(0))
    .select("shop", "location", "country", "id", "ean", "categories",
      "downloaded_on", "price.regular", "price.promo", "price.unit")
    .withColumnRenamed("regular", "regular_price")
    .withColumnRenamed("promo", "promo_price")
    .drop("eans")
    .withColumn("categories_tmp", col("categories").cast(DataTypes.StringType))
    .drop("categories")
    .withColumnRenamed("categories_tmp", "categories")
    .dropDuplicates("id", "regular_price", "promo_price", "unit", "downloaded_on")

  val productIdWithMostRecentTimestamp = price
    .groupBy("id")
    .max("downloaded_on")
    .withColumnRenamed("max(downloaded_on)","downloaded_on")

  val priceWithMostRecentTimestamp = productIdWithMostRecentTimestamp
    .join(price,
      Seq("id", "downloaded_on"),
      "left")

  priceWithMostRecentTimestamp
    .select("shop", "location", "country", "id", "regular_price", "promo_price",
      "unit", "ean", "categories")
    .withColumn("shop_partition", col("shop"))
    .repartition(1)
    .write
    .partitionBy("shop_partition")
    .mode("overwrite")
    .option("header", true)
    .csv("/home/luis/Dropbox/Nova pasta/daltix challenge/output/12")

  spark.stop()
}