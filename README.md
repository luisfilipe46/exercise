This query and diagram were made before the talk. In the talk the diagram was changed in order to the shop and unit be a field in the Price Fact table instead of a dimension table.


Diagram https://photos.app.goo.gl/jzwqAtLD2DtAHbou8

We can use Hive on Spark/Tez to build the distributed data warehouse.

Example query "Per shop, location how many products (use the product_id) were scraped per day."

select s.name, l.name, count(distinct prod.id)
from Price prc
inner join Product prod ON (prc.product_id = prod.id)
inner join Shop s ON (prc.shop_id = s.id)
inner join Location l ON (prc.location_id = l.id)
inner join Date d ON (prc.date_id = d.id)
where d.day = DAY(GETDATE()) AND d.month = MONTH(GETDATE()) AND YEAR(GETDATE())
group by s.id, l.id;

If something is not clear, please send me an email.